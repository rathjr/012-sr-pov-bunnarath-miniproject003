import React, { useEffect, useState } from "react";
import { useDispatch, useSelector, useStore } from "react-redux";
import { deleteArticle, fetchArticle } from "../redux/Action/ArticleAction";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router";
import { fetchCategory } from "../redux/Action/CategoryAction";
import ReactLoading from 'react-loading';
import ReactPaginate from 'react-paginate';
import { strings } from "../localization/localize";
import { GoogleLogin } from 'react-google-login';

export default function Home() {
  
  const [name, setName] = useState('')
  const [profile, setProfile] = useState('')

  const dispatch = useDispatch();
  const onDelete = bindActionCreators(deleteArticle, dispatch);
  const article = useSelector((state) => state.ArticleReducer);
  const category = useSelector((state) => state.CategoryReducer.categories);

  const history = useHistory();
  const [page, setPage] = useState(1)

  const [isArticleLoading, setIsArticleLoading] = useState(true)
  const [isCategoryLoading, setIsCategoryLoading] = useState(true)

  useEffect(() => {
    dispatch(fetchArticle(page)).then(()=>{
      setIsArticleLoading(false)
    });
    dispatch(fetchCategory()).then(()=>{
      setIsCategoryLoading(false)
    });
  }, []);

  const Loading = () => (
    <ReactLoading className='loading' type={"spokes"} color={"gray"} />
  );

  const onPageChange= ({selected})=>{
     dispatch(fetchArticle(selected+1))
 }

 const responseGoogle = (response) => {
   setName(response.profileObj.name)
   setProfile(response.profileObj.imageUrl)
 }
  return (
    <Container className="my-4">
      {isArticleLoading && isCategoryLoading ? Loading() :
      <Row>
        <Col md="3">
          <Card>
            <Card.Img variant="top"
              style={{ objectFit: "cover", height: "250px" }} 
              src={profile !== ''? profile : "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxARDhAOEBAPEBERDxERDhUPDxAVEA8RFxEXGBYSExYYHSggGBolHRMTIjEhJykrLi4uFx8zODMtNygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAwEBAQEAAAAAAAAAAAAAAwUGBAECB//EADsQAAIBAQQFCgQEBgMAAAAAAAABAgMEBRExEiEiQVEGE2FxgZGhscHRMkJSsmJyguEjJHOSotJDY/D/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A/cQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEdavCCxnKMV0tIrq1/UV8OlPqWC8QLUGeqco38tNL80n6IhfKGtujTXZL3A04MuuUNb6af8AbL3JqfKOXzU4vqk16MDRAqKPKCk/iU4dmK8NfgWNntVOeuE4y6nr7gJgAAAAAAAAAAAAAAAAAAAAAAAACmvW+lDGFPCUsm/lj7sCxtdtp0ljOWHBb31IoLbf1SWKprm1xzn7IqqlRyblJuTebeZ8gezm5PGTbfFttngAAAAAAACeDxWp7sMwALOx33VhgpfxI/i+LsfuaCw3jTq/C8Jb4vVJe5jBFtPFPBrJrNAb4FBdd+ZQrdSn/t7l8mB6AAAAAAAAAAAAAAAAAU1/XloLmoPaktpr5Y+7Agvq986VJ9E5L7Y+5QgAAAAAAAAAAAAAAAAAC2ua9nTapzexuf0fsVIA3qe89M9yfvLBqhN6v+Nvd+H2NCAAAAAAAAAAAAAAc9vtSpU5VHuyXF7kYupUcpOUni28Wy15R2vSqKmsoZ/mfsvUqAAAAAAAASUKMpy0YrF+S4vggIwXtluOK11G5PhHVHvzfgd8LDSWVOHbFN+IGTBrZWOk86cP7UcVpuSD1wbg++PuBnwTWqyzpy0ZrDg9z6mQgAAAAABPflwNhc9t52km/ijsz6+PaY877ltfN1li9mezL0feBrwAAAAAAAAAAI7RVUISm8oxbJCq5R1sKGj9ckuxa/RAZec3JuTzbbfW8zwAAAAAAAks9FzkoRzfcuLfQamx2WNKOjH9T3yfFnByfs+EHVecnhH8qz8fItgAAAAACOvRjOLhJYp+D4rpMtbbK6U3B698XxXE1pX31Z9Ok5b4bS6t67vIDNgAAAAAAA2l12jnKMJ78MJfmWpnUUXJets1KfBqS7Vg/JF6AAAAAAAAAM9ypntU49En4pejNCZjlO/40V/1r7pAVAAAAAAAANbYIYUaa/AvFY+pOQ2KWNKm/wAEfImAAAAAAB5KOKa4prvPQ3qx4AYtrcBJ4tviwAAAAAAWvJueFdr6oSXc0/c1JkLif8zT/V9jNeAAAAAAAAAMxymX8eP9NfdI05neVMNunLjGS7mvcCjAAAAAAABobhr6VPQ3wf8Ai9a9SzMjYrS6U1NdTXFb0auhWjOKlF4p/wDsH0gfYAAAAAcd7V9CjLjLZj25+GJ1zmknJtJJYtvcjL3nbOdnitUY6oLo4vrA5AAAAAAAAd9xL+Zp/q+xmvMrychjaMfphJ+S9TVAAAAAAAAACo5S0saKl9E13PV54FuQ2yjp05w+qLXbu8QMOA008Hqa1PoYAAAAAAB0WO2zpPGL1P4k8mR0KE5vCEXJ9G7re4s6VxSaxlNRe5JY97A7rLe1Keb0Hwll2PI7otPJp9RmLRdlWHy6S4w1+GZy61xXegNk3hnqOO03nSh8yk+ENb78kZjFve33s6aF3VZ5QaXGWyvED23XhOrqezFZRXm+JyFxK4ZaOqonLenF4dj/AGK202WdN4Ti1weafUwIQAAAAAAAX/JalqqVOqK835ovziuez6FCCebWlLrev2O0AAAAAAAAAAAMpygsuhW0l8NTa/V8y9e0rDZ3pY+dpOHzLXB8JGMkmm09TTwae5gAAALW7rocsJ1MYx3L5pLp4Imue7dSq1F0wi/uZcgfNKnGK0YpRXBI+gAAaAA8SPQAB5KKaaaTTzTWKZ6AKW8LmzlS7Y/6v0KVm0Ky9rt005wW2s0vn/cDPAAAdd1WXna0Y4bK2p9S3dupHIay4rFzdPFrbng30LcgLJAAAAAAAAAAAAABQcobuzrwX9RL7vcvzxoDBFhc1i5yelJbMP8AKW5E98XS4PTppuDetLOD9i2sVnVOnGHBa+mW8CcAAAAAAAAAAAAAAAFFftiwfPRWpvCfQ/q7SoNjWpqUXF5STTKGwXRKdRqeKhCWEn9XRH3AkuG7tOXOyWxF7P4pL0Rpz5pwUUopYJLBJbkfQAAAAAAAAAAAAAAAAHmBHOnvRKAOUHRKCZDKDQHyAAAAAAAAAAAPqMGyWNNLpA+IU+JKkegAAAAAAAAAAAAAAAAAAAAAAAAD5cEz4dLpJQBA6TPObfA6ABz82+B6qTJwBEqXSfagkfQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/9k="} />
          </Card>
        </Col>
        <Col md="9">
          <h1>{strings.Category}</h1>
          <Row className="my-3">
            {category.map((item, index) => (
                <Button key={index} className="mx-1" variant="outline-secondary" size="sm">{item.name}</Button>
            ))}
          </Row>
          <Row className="my-3">
            {article.articles.map((item, index) => (
              <Col key={index} md="4">
                <Card>
                  <Card.Img variant="top" style={{ objectFit: "cover", height: "150px" }} src={item.image} />
                  <Card.Body>
                    <Card.Title>{item.title}</Card.Title>
                    <Card.Text className="text-line-3">{item.description}</Card.Text>
                    <Button variant="primary" size='sm' onClick={()=>history.push('/view/'+item._id)}>{strings.Read}</Button>{" "}
                    <Button 
                    variant="warning" 
                    size='sm'
                    onClick={()=>{
                      history.push('/update/article/'+item._id)
                    }}
                    >{strings.Edit}</Button>{" "}
                    <Button variant="danger" size='sm' onClick={()=> onDelete(item._id)}>{strings.Delete}</Button>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
          <ReactPaginate
                pageCount={article.totalPage}
                onPageChange={onPageChange}
                containerClassName="pagination pagination-sm justify-content-center"
                pageClassName="page-item btn btn-sm"
                pageLinkClassName="page-link"
                previousClassName="page-item btn btn-sm"
                previousLinkClassName="page-link"
                nextLinkClassName="page-link"
                nextClassName="page-item btn btn-sm"
                activeClassName="active"
            />
        </Col>
      </Row>
      }
    </Container>
  );
}
