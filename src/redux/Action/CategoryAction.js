import api from "../../Api/api"

export const fetchCategory = () => async dp => {

    let response = await api.get('/category')

    return dp({
        type: "FETCH_CATEGORY",
        payload: response.data.data
    })
}

export const postCategory = (category) => async dp => {

    let response = await api.post('/category', category)

    return dp({
        type: "POST_CATEGORY",
        payload: response.data.message
    })
}

export const deleteCategory = (id) => async dp => {

    let response = await api.delete('/category/'+id )

    return dp({
        type: "DELETE_CATEGORY",
        payload: response.data.data
    })
}

export const updateCategory = (id, newCategory) => async dp => {

    let response = await api.put('/category/'+id, newCategory)

    return dp({
        type: "UPDATE_CATEGORY",
        payload: response.data.message
    })
}